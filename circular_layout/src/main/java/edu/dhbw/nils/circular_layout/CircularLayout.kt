package edu.dhbw.nils.circular_layout

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup

class CircularLayout(context: Context, attributeSet: AttributeSet) : ViewGroup(context, attributeSet) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        for (child in (0 until childCount).map { getChildAt(it) }){
            if (child.visibility != View.GONE) {
                measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0)

            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}