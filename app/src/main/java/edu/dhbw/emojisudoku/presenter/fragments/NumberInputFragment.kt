package edu.dhbw.emojisudoku.presenter.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.util.None
import edu.dhbw.emojisudoku.util.asOptional
import edu.dhbw.emojisudoku.presenter.GameInputPresenter
import kotlinx.android.synthetic.main.input_fragment.*

class NumberInputFragment : Fragment() {

    private val model: SudokuModel
        get() = ((activity ?: parentFragment) as GameInputPresenter).model

    private val presenter: GameInputPresenter
        get() = ((activity ?: parentFragment) as GameInputPresenter)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.input_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        model.mappings.mapIndexed { index, emoji ->
            val par = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT)
            par.weight = 1F
            val btn = Button(context)
                    .apply {
                        text = emoji
                        layoutParams = par
                    }

            btn.setOnClickListener {
                presenter.userInputSubject.onNext(if (index != 0) index.asOptional() else None)
            }

            input_button_layout.addView(btn)
        }
    }
}

