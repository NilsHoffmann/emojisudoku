package edu.dhbw.emojisudoku.presenter.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.util.Optional
import edu.dhbw.emojisudoku.view.gameGrid.HintingGameView
import edu.dhbw.emojisudoku.presenter.SudokuPresenter
import io.reactivex.subjects.PublishSubject

class HintingGameViewFragment : Fragment(){
    private val model: SudokuModel
        get() = ((activity ?: parentFragment) as SudokuPresenter).model

    val selectedCellSubject: PublishSubject<Optional<Int>> = PublishSubject.create()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val gameView = HintingGameView(model, context!!, attributeSet = null)
        gameView.selectedCellSubject = selectedCellSubject
        return gameView
    }

}