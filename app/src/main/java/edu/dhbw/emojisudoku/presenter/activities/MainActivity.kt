package edu.dhbw.emojisudoku.presenter.activities

import android.content.Intent
import android.graphics.ColorSpace
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.Difficulty
import edu.dhbw.emojisudoku.dataModel.SudokuGenerator
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val m = SudokuGenerator.generate(9, Difficulty.NORMAL)
        val mod = SudokuModel(m, difficulty = Difficulty.NORMAL)
        sudokuMainView.recycle(mod)

        btn_newgame.setOnClickListener { startActivity(Intent(this, GameActivity::class.java)) }
        btn_highscores.setOnClickListener { startActivity(Intent(this, HighscoreActivity::class.java)) }
        btn_settings.setOnClickListener { startActivity(Intent(this, SettingsActivity::class.java)) }
        btn_resume.setOnClickListener { startActivity(Intent(this, ResumeActivity::class.java)) }

    }
}
