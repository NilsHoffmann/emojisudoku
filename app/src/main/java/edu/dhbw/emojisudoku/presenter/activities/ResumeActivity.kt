package edu.dhbw.emojisudoku.presenter.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.AnticipateOvershootInterpolator
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.presenter.PreviewPresenter
import edu.dhbw.emojisudoku.presenter.fragments.GameViewFragment
import edu.dhbw.emojisudoku.presenter.fragments.ResumeSelectionFragment
import kotlinx.android.synthetic.main.activity_game.*

class ResumeActivity : AppCompatActivity(), PreviewPresenter {

    override lateinit var model: SudokuModel

    private var currentState: GameActivity.State = GameActivity.State.SELECTOR

    private val resumeFragment = ResumeSelectionFragment()
    private val gameViewFragment = GameViewFragment()


    override fun onSelected(model: SudokuModel) {
        if (currentState == GameActivity.State.SELECTOR) {
            currentState = GameActivity.State.SELECTOR_PREVIEW

            //show the preview as well
            this.model = model
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.central_fragment_container, gameViewFragment)
                    .addToBackStack(null)
                    .commit()


            start_btn.visibility = View.VISIBLE
            start_btn.animate()
                    .translationY(0f)
                    .apply {
                        interpolator = AnticipateOvershootInterpolator()
                        duration = 500
                    }
                    .start()

            start_btn.setOnClickListener {
                startActivity(Intent(this, GameInputActivity::class.java)
                        .apply { putExtra("model", model) })
            }


        } else {
            //show a different preview
            this.model.data = this.model.data
            this.model.mappings = this.model.mappings

            start_btn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))

            gameViewFragment.gameView!!.recycle(this.model)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resume)

        supportFragmentManager.beginTransaction()
                .add(R.id.lower_fragment_container, resumeFragment)
                .commit()
    }
}
