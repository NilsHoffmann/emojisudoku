package edu.dhbw.emojisudoku.presenter.activities

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.AnticipateOvershootInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.OvershootInterpolator
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.presenter.PreviewPresenter
import edu.dhbw.emojisudoku.presenter.fragments.GameViewFragment
import edu.dhbw.emojisudoku.presenter.fragments.PreviewSelectionFragment
import kotlinx.android.synthetic.main.activity_game.*

class GameActivity : AppCompatActivity(), PreviewPresenter {

    override var model: SudokuModel = SudokuModel()

    private val gameViewFragment = GameViewFragment()
    private val previewSelectionFragment = PreviewSelectionFragment()


    private var currentState: State = State.SELECTOR

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        //start with just the preview in vertical layout
        supportFragmentManager.beginTransaction()
                .add(R.id.lower_fragment_container, previewSelectionFragment)
                .commit()
    }

    override fun onSelected(model: SudokuModel) {
        if (currentState == State.SELECTOR) {
            currentState = State.SELECTOR_PREVIEW

            //show the preview as well
            this.model = model
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.central_fragment_container, gameViewFragment)
                    .addToBackStack(null)
                    .commit()

            start_btn.visibility = View.VISIBLE
            start_btn.animate()
                    .translationY(0f)
                    .apply {
                        interpolator = AnticipateOvershootInterpolator()
                        duration = 500
                    }
                    .start()

            start_btn.setOnClickListener {
                startActivity(Intent(this, GameInputActivity::class.java)
                        .apply { putExtra("model", model) })
            }


        } else {
            //show a different preview
            this.model.data = model.data
            this.model.mappings = model.mappings

            gameViewFragment.gameView!!.recycle(this.model)

            start_btn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))
        }
    }


    enum class State {
        SELECTOR, SELECTOR_PREVIEW
    }

}
