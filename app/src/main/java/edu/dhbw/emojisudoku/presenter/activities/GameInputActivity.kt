package edu.dhbw.emojisudoku.presenter.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.AnticipateOvershootInterpolator
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.*
import edu.dhbw.emojisudoku.presenter.GameInputPresenter
import edu.dhbw.emojisudoku.presenter.fragments.HintingGameViewFragment
import edu.dhbw.emojisudoku.presenter.fragments.NumberInputFragment
import edu.dhbw.emojisudoku.util.None
import edu.dhbw.emojisudoku.util.Optional
import edu.dhbw.emojisudoku.util.asOptional
import edu.dhbw.emojisudoku.util.value
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_game_input.*
import org.jetbrains.anko.doAsync
import java.util.*
import kotlin.concurrent.timer
import kotlin.math.roundToInt

class GameInputActivity : AppCompatActivity(), GameInputPresenter {

    override val userInputSubject: PublishSubject<Optional<Int>> = PublishSubject.create()

    override var model: SudokuModel = SudokuModel()
    private val disposables = mutableListOf<Disposable>()
    private val timers = mutableListOf<Timer>()

    private var selectedCell: Optional<Int> = None

    private lateinit var state: PersistentSudoku

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        state = savedInstanceState?.getParcelable("state") ?: return
        model = state.model
        model.sudokuChangedSubject.onNext(model.data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_input)


        if (!::state.isInitialized) {
            model = when {
                intent?.type?.equals("emojisudoku")
                        ?: false -> parseExternalIntent(intent.getStringExtra(Intent.EXTRA_TEXT))
                intent.extras?.get("model") != null -> intent!!.extras!!.get("model") as SudokuModel
                else -> throw Exception("Model in intent missing")
            }

            val persistent = when (model.persistenceId) {
                null -> PersistentSudoku(model, Calendar.getInstance().time, 0)
                        .apply { doAsync { this@apply.recordGame(applicationContext.gameDatabase) } }
                else -> applicationContext.gameDatabase.getGame(model.persistenceId!!) //blocking call
            }
            state = persistent
        }


        val gameViewFragment = HintingGameViewFragment()
        val inputFragment = NumberInputFragment()
        //replace fragments
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.central_fragment_container, gameViewFragment)
                .replace(R.id.lower_fragment_container, inputFragment)
                .commit()


        //show the input if something is selected
        disposables.add(gameViewFragment.selectedCellSubject.subscribe { cell ->
            selectedCell = cell
            when {
                cell.isPresent ->
                    supportFragmentManager.beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .show(inputFragment)
                            .commit()
                else ->
                    supportFragmentManager.beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .hide(inputFragment)
                            .commit()
            }
        })

        //modify data to user input
        disposables.add(userInputSubject.subscribe { input ->
            if (selectedCell == None) {
                Log.e("GameInput", "User Input on hidden input field shouldn't happen and will be ignored")
                return@subscribe
            }

            //add move to history stack
            handleInput(selectedCell.value!!, input, state)
        })


        //react to undo/redo buttons
        undo_btn.setOnClickListener {
            val undo = state.undoStack.pop()

            model.data.applyMove(undo.getInverseMove())
            model.sudokuChangedSubject.onNext(model.data)

            state.redoStack.push(undo)
            redo_btn.isEnabled = true
            undo_btn.isEnabled = !state.undoStack.empty()
        }

        redo_btn.setOnClickListener {
            val redo = state.redoStack.pop()

            model.data.applyMove(redo.getMove())
            model.sudokuChangedSubject.onNext(model.data)

            state.undoStack.push(redo)
            undo_btn.isEnabled = true
            redo_btn.isEnabled = !state.redoStack.empty()

        }


        //time to show how long the current game was played
        val t = timer(period = 1000L) {
            runOnUiThread {
                val seconds = state.timePlayed
                game_time.text = String.format("%02d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 60))
            }
            state.timePlayed += 1
            doAsync { state.recordTime(applicationContext.gameDatabase) }
        }
        timers.add(t)

        //Hint Button
        hint_btn.setOnClickListener {
            hint_btn.isEnabled = false
            doAsync {
                val toSolve = SudokuData(state.model.data)
                val solved = SudokuSolver.solve(toSolve).first
                        ?: throw RuntimeException("Couldnt solve Sudoku")
                val displayedIdx = (0 until solved.linear.size).first { i ->
                    solved.getCell(i).selected != state.model.data.getCell(i).selected
                }
                //handle like normal input
                state.hintsTaken += 1
                state.recordHintsTaken(applicationContext.gameDatabase)

                runOnUiThread {
                    handleInput(displayedIdx, solved.getCell(displayedIdx).selected.asOptional(), state)
                    hint_btn.isEnabled = true
                }
            }
        }

    }

    private fun parseExternalIntent(str: String?): SudokuModel {
        if (str == null) return SudokuModel()

        val state = str.substringAfter("game=")
        val gamestate = state.substringBefore(":emoji=")
        val emojistr = state.substringAfter(":emoji=").substringBefore(":diff=")
        val diff = Integer.parseInt(state.substringAfter(":diff="))

        val data = parseString(gamestate)
        val mappings = NumberEmojiMappings.fromString(emojistr)
        return SudokuModel(data, mappings, Difficulty.from(diff))
    }

    private fun handleInput(cellIdx: Int, input: Optional<Int>, persistent: PersistentSudoku) {
        //Updates the Sudoku while handling the undo stack and gameDatabase persistence
        val move = UndoableMove(cellIdx, input.value, model.data.getCell(cellIdx).selected)
        persistent.undoStack.push(move)
        undo_btn.isEnabled = true

        model.data.getCell(cellIdx).selected = input.value
        model.sudokuChangedSubject.onNext(model.data)

        checkFinished(persistent)
        doAsync { persistent.recordMove(this@GameInputActivity.gameDatabase, Move(cellIdx, input.value)) }
    }

    private fun onFinished(sudoku: PersistentSudoku) {
        Log.i("GameInput", "sudoku finished")
        finished_layout.visibility = View.VISIBLE
        finished_layout.animate()
                .translationY(0f)
                .apply { interpolator = AnticipateOvershootInterpolator() }
                .start()

        timers.forEach { it.cancel() }

        val seconds = sudoku.timePlayed
        finished_time.text = String.format("%02d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 60))


        finished_score.text = calcScore(sudoku).toString()
        doAsync {
            sudoku.recordHighscore(baseContext.gameDatabase, calcScore(sudoku), Calendar.getInstance().time)
        }

        finished_return.setOnClickListener {
            startActivity(Intent(baseContext, MainActivity::class.java))
        }
    }

    private fun checkFinished(sudoku: PersistentSudoku) {
        if (sudoku.model.data.possible() && sudoku.model.data.fullySelected()) {
            onFinished(sudoku)
        }
    }

    private fun calcScore(sudoku: PersistentSudoku): Int {
        val base = when (sudoku.model.difficulty) {
            Difficulty.TRIVIAL -> resources.getInteger(R.integer.TRIVIAL)
            Difficulty.EASY -> resources.getInteger(R.integer.EASY)
            Difficulty.NORMAL -> resources.getInteger(R.integer.NORMAL)
            Difficulty.HARD -> resources.getInteger(R.integer.HARD)
        }

        //🤔
        return ((base / sudoku.hintsTaken - sudoku.timePlayed / 10f) * 1000).roundToInt()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putParcelable("state", state)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.forEach { it.dispose() }
        timers.forEach { it.cancel() }
    }


}
