package edu.dhbw.emojisudoku.presenter

import edu.dhbw.emojisudoku.dataModel.PersistentSudoku
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.util.Optional
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

interface SudokuPresenter {
    val model: SudokuModel
}

interface PreviewPresenter: SudokuPresenter {
    fun onSelected(model: SudokuModel)
}

interface GameInputPresenter: SudokuPresenter {
    val userInputSubject: PublishSubject<Optional<Int>>

}

