package edu.dhbw.emojisudoku.presenter.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.view.gameGrid.GameView
import edu.dhbw.emojisudoku.presenter.SudokuPresenter

class GameViewFragment() : Fragment() {

    private val model: SudokuModel
        get() = ((activity ?: parentFragment) as SudokuPresenter).model

    var gameView: GameView? = null
    var onClickListener: View.OnClickListener? = null
        set(value) {
            field = value
            if (gameView != null) {
                gameView!!.setOnClickListener(value)
            }
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        gameView = GameView(model, context!!, attributeSet = null)
        if (onClickListener != null) {
            gameView!!.setOnClickListener(onClickListener)
        }
        return gameView
    }

}