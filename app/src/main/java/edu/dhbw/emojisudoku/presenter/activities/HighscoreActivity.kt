package edu.dhbw.emojisudoku.presenter.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.Difficulty
import edu.dhbw.emojisudoku.dataModel.gameDatabase
import edu.dhbw.emojisudoku.view.gameGrid.GameView
import kotlinx.android.synthetic.main.activity_highscore.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.sdk27.coroutines.onClick

class HighscoreActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_highscore)



        high_scores_recycler_view.layoutManager = LinearLayoutManager(this)
        val adapter = HighscoreAdapter()
        high_scores_recycler_view.adapter = adapter

        doAsync {
            val scores = baseContext.gameDatabase.getHighscores()
            adapter.dataset = scores
            adapter.notifyDataSetChanged()
        }
    }
}


class HighscoreAdapter : RecyclerView.Adapter<HighscoreViewHolder>() {

    var dataset: List<Highscore> = ArrayList()

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(viewholder: HighscoreViewHolder, position: Int) {
        viewholder.bindView(dataset[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): HighscoreViewHolder {
        return HighscoreViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.layout_high_score, parent, false))
    }

}

class HighscoreViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bindView(highscore: Highscore) {

        view.findViewById<TextView>(R.id.difficulty).text = highscore.difficulty.toString()
        view.findViewById<TextView>(R.id.score).text = highscore.score.toString()
        view.findViewById<TextView>(R.id.time).text = String.format("%02d:%02d:%02d",
                highscore.time / 3600, (highscore.time % 3600) / 60, (highscore.time % 60))

        val detailLayout = view.findViewById<LinearLayout>(R.id.detail_layout)
        val preview = view.findViewById<GameView>(R.id.highscore_game_view)
        val shareBtn = view.findViewById<Button>(R.id.share_btn)

        doAsync {
            val game = view.context.gameDatabase.getGame(highscore.persistenceId)
            view.context.runOnUiThread {
                preview.recycle(game.model)

                shareBtn.onClick {
                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT,
                                "emojisudoku://game=${game.model.data.getInitialStateStr()}:emoji=${game.model.mappings}:diff=${game.model.difficulty}")
                        type = "text/plain"
                    }
                    startActivity(sendIntent)
                }

            }
        }


        //expand detail layout
        var expanded = false
        view.setOnClickListener {
            if (!expanded) {
                expanded = true
                view.isClickable = false
                detailLayout.visibility = View.VISIBLE
                detailLayout.animate().translationY(0f)
                        .apply { duration = 250 }
                        .withEndAction { view.isClickable = true }
                        .start()
            } else {
                expanded = false
                view.isClickable = false
                detailLayout.animate().translationY(-10000f)
                        .apply { duration = 250 }
                        .withEndAction { detailLayout.visibility = View.GONE; view.isClickable = true }
                        .start()
            }
        }
    }
}

data class Highscore(val difficulty: Difficulty, val score: Long, val time: Long,
                     val finishedOn: Long, val persistenceId: Long)
