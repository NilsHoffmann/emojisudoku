package edu.dhbw.emojisudoku.presenter.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.Difficulty
import edu.dhbw.emojisudoku.dataModel.PersistentSudoku
import edu.dhbw.emojisudoku.dataModel.gameDatabase
import edu.dhbw.emojisudoku.presenter.PreviewPresenter
import edu.dhbw.emojisudoku.view.gameList.GameSelectorAdapter
import kotlinx.android.synthetic.main.preview_selection_fragment.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk27.coroutines.onCheckedChange
import org.jetbrains.anko.support.v4.runOnUiThread
import kotlin.math.abs

class ResumeSelectionFragment : Fragment() {
    private val layoutManager = LinearLayoutManager(context)

    var selected: PersistentSudoku? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view = inflater.inflate(R.layout.preview_selection_fragment, container, false)

        view.addOnLayoutChangeListener { _, left, top, right, bottom,
                                         _, _, _, _ ->
            //that method signature...
            if (abs(bottom - top) > abs(right - left)) {
                //view is longer than wide
                layoutManager.orientation = LinearLayout.VERTICAL
            } else {
                layoutManager.orientation = LinearLayout.HORIZONTAL
            }
        }
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        difficultyFilters = mapOf(
                Difficulty.TRIVIAL to diff_trivial,
                Difficulty.EASY to diff_easy,
                Difficulty.NORMAL to diff_medium,
                Difficulty.HARD to diff_hard)

        choose_new_game.text = resources.getString(R.string.resume_game)

        initRecyclerView(game_selection_recycler_view)
        super.onViewCreated(view, savedInstanceState)
    }


    private lateinit var difficultyFilters: Map<Difficulty, CheckBox>
    private val selectedDifficulty: Set<Difficulty>
        get() {
            return difficultyFilters.filter { it.value.isChecked }.keys
        }


    private fun initRecyclerView(v: RecyclerView) {
        val adapter = GameSelectorAdapter(activity as PreviewPresenter)

        if (v.layoutManager !is LinearLayoutManager) {
            v.layoutManager = layoutManager
        }

        v.adapter = adapter

        difficultyFilters.values.map {
            it.onCheckedChange { _, _ ->
                adapter.filterFunc = { sudokuModel -> sudokuModel.difficulty in selectedDifficulty }
            }
        }


        doAsync {
            val playedGames = context!!.gameDatabase.getGames(30)
                    .map { it.model.apply { persistenceId = it.persistenceId } }

            runOnUiThread {
                adapter.displayDataSet.addAll(playedGames)
                adapter.notifyDataSetChanged()
            }

        }
    }

}