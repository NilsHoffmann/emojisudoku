package edu.dhbw.emojisudoku.presenter.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.Difficulty
import edu.dhbw.emojisudoku.dataModel.generateRandom
import edu.dhbw.emojisudoku.presenter.PreviewPresenter
import edu.dhbw.emojisudoku.view.gameList.EndlessRecyclerOnScrollListener
import edu.dhbw.emojisudoku.view.gameList.GameSelectorAdapter
import kotlinx.android.synthetic.main.preview_selection_fragment.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk27.coroutines.onCheckedChange
import org.jetbrains.anko.support.v4.runOnUiThread
import kotlin.math.abs


class PreviewSelectionFragment : Fragment() {

    private val layoutManager = LinearLayoutManager(context)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view = inflater.inflate(R.layout.preview_selection_fragment, container, false)

        view.addOnLayoutChangeListener { v, left, top, right, bottom,
                                         _, _, _, _ ->
            //that method signature...
            if (abs(bottom - top) > abs(right - left)) {
                //view is longer than wide
                layoutManager.orientation = LinearLayout.VERTICAL
            } else {
                layoutManager.orientation = LinearLayout.HORIZONTAL
            }
        }
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        difficultyFilters = mapOf(
                Difficulty.TRIVIAL to diff_trivial,
                Difficulty.EASY to diff_easy,
                Difficulty.NORMAL to diff_medium,
                Difficulty.HARD to diff_hard)


        initRecyclerView(game_selection_recycler_view)
        super.onViewCreated(view, savedInstanceState)
    }


    private lateinit var difficultyFilters: Map<Difficulty, CheckBox>
    val selectedDifficulty: Set<Difficulty>
        get() {
            return difficultyFilters.filter { it.value.isChecked }.keys
        }


    private fun initRecyclerView(v: RecyclerView) {
        val adapter = GameSelectorAdapter(activity as PreviewPresenter)
        v.layoutManager = layoutManager
        v.adapter = adapter

        difficultyFilters.values.map {
            it.onCheckedChange { _, _ ->
                adapter.filterFunc = { sudokuModel -> sudokuModel.difficulty in selectedDifficulty }
            }
        }


        doAsync {
            repeat(30) {
                val gen = generateRandom(selectedDifficulty)
                runOnUiThread {
                    adapter.displayDataSet.add(gen)
                    adapter.notifyDataSetChanged()
                    Log.i("EndlessPreview", "End of preview reached, added new")
                }
            }

        }

        //add new items when needed
        v.addOnScrollListener(object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {

                doAsync {
                    val gen = (0..10).map { generateRandom(selectedDifficulty) }
                    runOnUiThread {
                        Log.i("EndlessPreview", "End of preview reached, generating new")
                        adapter.displayDataSet.addAll(gen)
                        adapter.notifyDataSetChanged()
                    }

                }
            }
        })
    }

}