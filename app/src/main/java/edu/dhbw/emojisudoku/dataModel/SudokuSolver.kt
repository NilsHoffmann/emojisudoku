package edu.dhbw.emojisudoku.dataModel

object SudokuSolver {

    private fun fixVals(sudoku: SudokuData): Pair<SudokuData?, Boolean> {

        if (!sudoku.possible())
            return Pair(null, false)
        if (sudoku.fullySelected())
            return Pair(sudoku, true)

        //fix some random cell
        val cs = sudoku.linear.mapIndexed { i, e -> i to e }
                .filter { !it.second.onePossible }
                .filter { it.second.selected == null }
                .sortedBy { it.second.possibleValues.size }

        val cIdx = cs.first().first


        val newSudoku = SudokuData(sudoku.linear.mapIndexed { index, cell ->
            if (index == cIdx)
                cell.copy(cell.possibleValues.first(), cell.possibleValues.toSet())
            else
                cell.copy(possibleValues = cell.possibleValues.toSet())
        }.chunked(sudoku.size))

        //test it
        var result = this.solve(newSudoku)
        if (!result.second) {
            //try other cell
            if (cs.size < 2) return Pair(null, false)
            val c2Idx = cs[1].first
            val new = SudokuData(sudoku.linear.mapIndexed { index, cell ->
                if (index == c2Idx)
                    cell.copy(cell.possibleValues.first(), cell.possibleValues.toSet())
                else
                    cell.copy(possibleValues = cell.possibleValues.toSet())
            }.chunked(sudoku.size))

            result = this.solve(new)
        }
        return result
    }


    fun solve(init: SudokuData): Pair<SudokuData?, Boolean> {
        var sudoku = init
        //TODO: Not exactly a great solution
        repeat(3) { sudoku = eliminateOnce(sudoku) }
        return fixVals(sudoku)
    }

    private fun eliminateOnce(sudoku: SudokuData): SudokuData {
        sudoku.rows.forEach { row ->
            filterRow(row)
        }
        sudoku.getAllColumnsAsRows().forEach { column ->
            filterRow(column)
        }

        sudoku.getSquares().forEach { square ->
            filterRow(square.value)
        }

        return sudoku
    }

    private fun filterRow(row: List<Cell>): List<Cell> {
        val fixedValues = row.mapNotNull { it.selected }
        row.forEach { cell ->
            if (cell.selected == null)
                cell.possibleValues -= fixedValues

            if(cell.possibleValues.size == 1){
                cell.selected = cell.possibleValues.first()
            }
        }
        return row
    }
}

