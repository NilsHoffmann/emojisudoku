package edu.dhbw.emojisudoku.dataModel

import android.util.Log
import java.util.*

object SudokuGenerator {
    fun generate(size: Int, difficulty: Difficulty): SudokuData {

        val fillPercentage = when (difficulty) {
            Difficulty.TRIVIAL -> .95
            Difficulty.EASY -> .8
            Difficulty.NORMAL -> .6
            Difficulty.HARD -> 0.4
        }
        val random = Random()
        val sudokuData = SudokuData(size)
        val fields = size * size - 1

        //fill some fields to create a seed for the solver
        val remaining = (1..sudokuData.size).toMutableList()
        repeat(remaining.size) {
            sudokuData.getCell(random.nextInt(fields))
                    .apply {
                        selected = remaining.removeAt(random.nextInt(remaining.size))
                        editable = false
                        possibleValues = setOf(selected!!)
                    }
        }

        val solved = SudokuSolver.solve(sudokuData)
        if (!solved.second) {
            Log.e("Generator", "Couldnt solve generated Sudoku!")
            throw RuntimeException("Couldnt solve generated Sudoku!")
        }
        val solvedS = solved.first!!


        //remove a number of fields according to difficulty
        solvedS.linear.forEach { cell ->
            if (random.nextFloat() > fillPercentage) {
                cell.selected = null
                cell.editable = true
            } else {
                cell.editable = false
            }
        }


        return SudokuData(solvedS)
    }
}