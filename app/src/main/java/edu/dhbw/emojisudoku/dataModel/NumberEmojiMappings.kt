package edu.dhbw.emojisudoku.dataModel

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

//TODO: Get this from somewhere

private val possibleEmoji =
            listOf("\uD83D\uDE02", "\uD83D\uDE00", "\uD83D\uDE42", "\uD83D\uDE1C", "\uD83E\uDD2F", "\uD83E\uDD14",
                    "\uD83D\uDC69\u200D\uD83C\uDF93", "\uD83D\uDD74", "\uD83D\uDC4C", "\uD83D\uDC08", "☄️", "\uD83C\uDF46", "\uD83E\uDD51")


@Parcelize
class NumberEmojiMappings constructor(private val mappings: List<String>) : Iterable<String>, Parcelable {

    override fun iterator(): Iterator<String> {
        return mappings.iterator()
    }

    companion object {
        fun random(amount: Int = 9) = NumberEmojiMappings(genRandom(amount).toMutableList().apply { add(0, "") })
        fun linear(amount: Int = 9) = NumberEmojiMappings((1..amount).map { it.toString() }.toMutableList().apply { add(0, "") })

        fun fromString(str: String): NumberEmojiMappings {
            return NumberEmojiMappings(str.split(", "))
        }

        private fun genRandom(amount: Int): List<String> {
            val remaining = possibleEmoji.toMutableList()
            val random = Random()
            return (1..amount).map { remaining.removeAt(random.nextInt(remaining.size)) }
        }
    }

    fun getString(cell: Cell): String {
        return mappings.get(cell.selected ?: 0)
    }

    override fun toString(): String {
        return mappings.joinToString(", ")
    }


    @IgnoredOnParcel
    val size = mappings.size

    fun get(idx: Int): String {
        return mappings[idx]
    }
}