package edu.dhbw.emojisudoku.dataModel

import android.os.Parcelable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SudokuModel(var data: SudokuData = SudokuData.zeros(),
                       var mappings: NumberEmojiMappings = NumberEmojiMappings.random(),
                       val difficulty: Difficulty = Difficulty.TRIVIAL,
                       var persistenceId: Long? = null) : Parcelable {
    //FIXME: having persistence Id here is terribly hacky and only done because of terrible design decisions

    @IgnoredOnParcel
    val sudokuChangedSubject: PublishSubject<SudokuData> = PublishSubject.create()

}