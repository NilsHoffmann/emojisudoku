package edu.dhbw.emojisudoku.dataModel

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import edu.dhbw.emojisudoku.presenter.activities.Highscore
import org.jetbrains.anko.db.*
import java.util.*

const val DATABASE_NAME = "games.db"
const val VERSION = 9

class GameDatabaseOpenHelper private constructor(context: Context) : ManagedSQLiteOpenHelper(context, DATABASE_NAME, null, VERSION) {

    companion object {
        private var instance: GameDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): GameDatabaseOpenHelper {
            if (instance == null) {
                instance = GameDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }


    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("GAMES", false,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "started_on" to INTEGER,
                "time_played" to INTEGER,
                "puzzle" to TEXT + NOT_NULL,
                "mappings" to TEXT,
                "difficulty" to INTEGER,
                "hints_taken" to INTEGER)

        db.createTable("MOVES", false,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "game" to INTEGER,
                "move_number" to INTEGER + NOT_NULL,
                "field" to INTEGER + NOT_NULL,
                "entry" to INTEGER,
                FOREIGN_KEY("game", "GAMES", "id"))

        db.createTable("HIGHSCORES", false,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "game" to INTEGER,
                "score" to INTEGER + NOT_NULL,
                "finished_on" to INTEGER,
                "time" to INTEGER,
                "difficulty" to INTEGER,
                FOREIGN_KEY("game", "GAMES", "id"))
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable("GAMES", true)
        db.dropTable("MOVES", true)
        db.dropTable("HIGHSCORES", true)
        onCreate(db)
    }

}

// wrapper to avoid non type safe SQL
class GameDatabase(private val sqlDB: GameDatabaseOpenHelper) {

    companion object {
        private var instance: GameDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): GameDatabase {
            if (instance == null) {
                instance = GameDatabase(GameDatabaseOpenHelper.getInstance(ctx.applicationContext))
            }
            return instance!!
        }
    }

    fun addGame(sudoku: PersistentSudoku) {

        val rowId = sqlDB.use {
            insert("GAMES",
                    "started_on" to sudoku.started_on.time,
                    "time_played" to sudoku.timePlayed,
                    "puzzle" to sudoku.model.data.getInitialStateStr(),
                    "mappings" to sudoku.model.mappings.toString(),
                    "hints_taken" to sudoku.hintsTaken,
                    "difficulty" to sudoku.model.difficulty.id)
        }

        when (rowId) {
            -1L -> Log.e("GameDatabase", "Error persisting game!")
            else -> sudoku.persistenceId = rowId
        }
    }

    fun addMove(sudoku: PersistentSudoku, move: Move) {
        sqlDB.use {
            val newMoveNumber = select("MOVES", "move_number")
                    .whereSimple("game = ${sudoku.persistenceId}")
                    .orderBy("move_number", SqlOrderDirection.ASC)
                    .limit(1)
                    .exec {
                        if (count == 0) {
                            0
                        } else {
                            moveToNext()
                            getInt(0) + 1
                        }
                    }

            val rowId = insert("MOVES",
                    "game" to sudoku.persistenceId,
                    "move_number" to newMoveNumber,
                    "field" to move.cellIdx,
                    "entry" to move.entered)
            if (rowId == -1L) Log.e("GameDatabase", "Error persisting move!")

        }
    }

    fun addHighscore(sudoku: PersistentSudoku, score: Int) {
        sqlDB.use {
            insert("HIGHSCORES",
                    "game" to sudoku.persistenceId,
                    "score" to score,
                    "time" to sudoku.timePlayed,
                    "difficulty" to sudoku.model.difficulty.id,
                    "finished_on" to sudoku.finishedOn?.time)
        }
    }

    private fun fromColumns(columns: Map<String, Any?>): PersistentSudoku {
        val puzzle = columns["puzzle"] as String
        val data = parseString(puzzle)
        val mappings = NumberEmojiMappings.fromString(columns["mappings"] as String)
        val difficulty = Difficulty.from((columns["difficulty"] as Long).toInt())
        val hintsTaken = columns["hints_taken"] as Long
        val id = columns["id"] as Long

        val model = SudokuModel(data, mappings, difficulty)
        val startedOn = Date(columns["started_on"] as Long)
        val timePlayed = columns["time_played"] as Long
        return PersistentSudoku(model, startedOn, timePlayed, hintsTaken.toInt(), persistenceId = id)
    }

    fun getMoves(gameId: Long): List<Move> {
        return sqlDB.use {
            select("MOVES", "move_number", "field", "entry")
                    .whereSimple("game = $gameId")
                    .orderBy("move_number")
                    .exec {
                        this.asMapSequence().map {
                            Move((it["field"] as Long).toInt(), (it["entry"] as Long).toInt())
                        }.toList()
                    }
        }
    }

    fun getStartedGame(id: Long): PersistentSudoku {
        return sqlDB.use {
            select("GAMES", "id", "started_on", "time_played", "difficulty", "hints_taken", "puzzle", "mappings")
                    .whereSimple("id = $id")
                    .exec {
                        this.asMapSequence().map { fromColumns(it) }.single()
                    }
        }

    }

    fun getGames(limit: Int = 30): List<PersistentSudoku> {

        val started = sqlDB.use {
            select("GAMES", "id", "started_on", "time_played", "difficulty", "puzzle", "hints_taken", "mappings")
                    .orderBy("started_on", SqlOrderDirection.DESC)
                    .limit(limit)
                    .exec {
                        this.asMapSequence().map {
                            fromColumns(it)
                        }.toList()
                    }
        }
        return started.map { game ->
            getMoves(game.persistenceId!!).forEach { move -> game.model.data.applyMove(move) }
            return@map game
        }

    }

    fun updateGameTime(game: PersistentSudoku) {
        sqlDB.use {
            update("GAMES", "time_played" to game.timePlayed)
                    .whereSimple("id = ${game.persistenceId}")
                    .exec()
        }
        Log.d("DB", "updated time to ${game.timePlayed}")
    }

    fun updateHintsTaken(game: PersistentSudoku) {
        sqlDB.use {
            update("GAMES", "hints_taken" to game.hintsTaken)
                    .whereSimple("id = ${game.persistenceId}")
                    .exec()
        }
        Log.d("DB", "updated hints taken to ${game.hintsTaken}")
    }


    fun getGame(id: Long): PersistentSudoku {

        val moves = getMoves(id)
        val game = getStartedGame(id)

        moves.forEach { move -> game.model.data.applyMove(move) }
        return game
    }


    fun getHighscores(): List<Highscore> {
        return sqlDB.use {
            select("HIGHSCORES", "game", "time", "score", "difficulty", "finished_on")
                    .orderBy("finished_on")
                    .exec {
                        this.asMapSequence().map {

                            val score = it["score"] as Long
                            val finishedOn = it["finished_on"] as Long
                            val difficulty = Difficulty.from((it["difficulty"] as Long).toInt())
                            val gameId = it["game"] as Long
                            val time = it["time"] as Long

                            Highscore(difficulty, score, time, finishedOn, gameId)
                        }.toList()
                    }
        }
    }
}

//allows access to the DB instance from any context
val Context.gameDatabase
    get() = GameDatabase.getInstance(applicationContext)
