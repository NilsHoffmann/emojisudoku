package edu.dhbw.emojisudoku.dataModel

import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import kotlin.math.sqrt

fun parseString(gridString: String): SudokuData {
    require(gridString.length == 81)
    val cells = gridString
            .map { c: Char ->
                when {
                    c.isDigit() -> Cell(c.toString().toInt())
                    c == '.' -> Cell((1..9).toSet())
                    else -> throw Exception("Invalid Input")
                }
            }
    val rows = cells.chunked(9)
    return SudokuData(9, rows)
}


@Parcelize
data class SudokuData(val size: Int = 9,
                      var grid: List<List<Cell>> = List(size) { List(size) { Cell((1..9).toSet()) } }) : Parcelable {

    //create deep copy
    constructor(data: SudokuData) :
            this(data.size, data.grid
                    .map { row ->
                        row.map { cell ->
                            cell.copy(possibleValues = cell.possibleValues.toSet())
                        }.toList()
                    }.toList())

    constructor(grid: List<List<Cell>>) : this(grid.size, grid)


    @IgnoredOnParcel
    val squareSize = sqrt(size.toDouble()).toInt()


    companion object {
        fun zeros(size: Int = 9) = SudokuData(size)
    }

    val rows get() = grid
    val linear get() = grid.flatMap { row -> row.map { it } }

    fun getColumn(idx: Int) = rows.map { it[idx] }

    fun getAllColumnsAsRows() = rows.indices.map { getColumn(it) }

    fun getSquares(): Map<Int, List<Cell>> {
        return linear.mapIndexed { index, cell -> Pair(index, cell) } //add indices
                .groupBy {
                    val idx = it.first

                    val row = idx / size
                    val column = idx % size
                    val squareX = column / squareSize
                    val squareY = row / squareSize
                    squareY * squareSize + squareX
                } //map squares to list of cells
                .mapValues { it.value.map { cell -> cell.second } } //remove indices from map
    }

    fun getCell(x: Int, y: Int): Cell = grid[x][y]
    fun getCell(idx: Int) = getCell(idx % size, idx / size)

    fun getInitialStateStr(): String = linear.joinToString("") { cell -> if (cell.editable) "." else cell.selected!!.toString() }
    fun getInitialState(): SudokuData = SudokuData(this.linear.map { cell -> if (cell.editable) Cell.new() else cell }.chunked(this.size))

    fun duplicateRow(): List<Boolean> {
        return rows.map { row ->
            row.mapNotNull { cell -> cell.selected }.distinct().size == row.count { it.selected != null }
        }
    }

    fun duplicateColumns(): List<Boolean> {
        return getAllColumnsAsRows().map { row ->
            row.mapNotNull { cell -> cell.selected }.distinct().size == row.count { it.selected != null }
        }
    }

    fun duplicateSquares(): Map<Int, Boolean> {
        val cells = linear.mapIndexed { index, cell -> Pair(index, cell) } //add indices
                .groupBy {
                    val idx = it.first

                    val row = idx / size
                    val column = idx % size
                    val squareX = column / squareSize
                    val squareY = row / squareSize
                    squareY * squareSize + squareX
                } //map squares to list of cells
                .mapValues { it.value.map { cell -> cell.second } } //remove indices from map


        return cells.mapValues { square ->
            square.value.mapNotNull { cell -> cell.selected }
                    .distinct().size == square.value.count { cell -> cell.selected != null } //distinct number of selected cells is equal to number of selected cells
        }
    }

    fun possible(): Boolean {
        //no cell without possible entry
        val impossibleCells = grid.map { row -> row.map { cell -> cell.possibleValues.isNotEmpty() } }.all { it.all { it } }

        //No duplicates in a row
        val duplicateRow = duplicateRow().all { it }

        //no duplicates in columns
        val duplicateColumn = duplicateColumns().all { it }

        //no duplicates in Square
        val duplicateSquares = duplicateSquares().all { it.value }

        return impossibleCells && duplicateColumn && duplicateRow && duplicateSquares
    }

    fun fullySelected(): Boolean {
        return linear.all { it.selected != null }
    }

    fun applyMove(move: Move) {
        getCell(move.cellIdx).selected = move.entered
    }
}

data class Move(val cellIdx: Int, val entered: Int?)
data class UndoableMove(val cellIdx: Int, val entered: Int?, val previous: Int?) {
    fun getInverseMove(): Move {
        return Move(cellIdx, previous)
    }

    fun getMove(): Move {
        return Move(cellIdx, entered)
    }
}

@Parcelize
data class Cell(var selected: Int?, var possibleValues: Set<Int>, var editable: Boolean) : Parcelable {
    constructor(single: Int) : this(single, setOf(single), false)
    constructor(possibleValues: Set<Int>) : this(null, possibleValues, true)


    companion object {
        fun new(): Cell {
            return Cell((1..9).toSet())
        }
    }


    val onePossible get() = possibleValues.size == 1
}

enum class Difficulty(val id: Int) {
    TRIVIAL(0), EASY(1), NORMAL(2), HARD(3);

    companion object {
        fun from(id: Int): Difficulty {
            return values().first { difficulty -> difficulty.id == id }
        }
    }
}

fun generateRandom(difficulties: Set<Difficulty>): SudokuModel {
    val difficulty = difficulties.shuffled().first()
    Log.i("SudokuGen", "Generated Sudoku with difficulty $difficulty")
    return SudokuModel(SudokuGenerator.generate(9, difficulty), NumberEmojiMappings.random(9), difficulty)
}


