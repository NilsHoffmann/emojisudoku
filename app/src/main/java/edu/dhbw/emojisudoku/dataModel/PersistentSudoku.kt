package edu.dhbw.emojisudoku.dataModel

import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*


@Parcelize
class PersistentSudoku(val model: SudokuModel, val started_on: Date = Date(), var timePlayed: Long,
                       var hintsTaken: Int = 0, var finishedOn: Date? = null, val score: Int? = null,
                       var persistenceId: Long? = null) : Parcelable {

    @IgnoredOnParcel
    val undoStack = Stack<UndoableMove>()
    @IgnoredOnParcel
    val redoStack = Stack<UndoableMove>()

    fun recordGame(database: GameDatabase) {
        database.addGame(this)
        Log.i("persistence", "persisted game")
    }

    fun recordTime(database: GameDatabase) {
        database.updateGameTime(this)
    }

    fun recordHintsTaken(database: GameDatabase) {
        database.updateHintsTaken(this)
    }

    fun recordMove(database: GameDatabase, move: Move) {
        if (persistenceId == null) {
            Log.w("persistence", "recorded move without recording game first!")
            recordGame(database)
        }
        database.addMove(this, move)
        Log.i("persistence", "wrote Input of ${move.entered} to cell ${move.cellIdx} to db")
    }

    fun recordHighscore(database: GameDatabase, score: Int, finishedOn: Date?) {
        this.finishedOn = finishedOn
        database.addHighscore(this, score)
    }
}

