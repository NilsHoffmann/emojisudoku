package edu.dhbw.emojisudoku.view.gameList

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.RelativeLayout
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.presenter.PreviewPresenter
import edu.dhbw.emojisudoku.view.gameGrid.GameView

class GameSelectorAdapter(private val presenter: PreviewPresenter) : RecyclerView.Adapter<GameGridViewHolder>() {

    val displayDataSet = mutableListOf<SudokuModel>()

    var filterFunc: (SudokuModel) -> Boolean = { true }
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    //TODO> Remove this /ViewHolder classes dependency on presenter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameGridViewHolder {


        val size = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT).apply { setMargins(48, 24, 48, 24) }


        val gameView = GameView(null, parent.context, null)

        gameView.layoutParams = size

        return GameGridViewHolder(gameView, presenter)
    }

    override fun getItemCount(): Int {
        return displayDataSet.filter(filterFunc).size
    }

    override fun onBindViewHolder(viewHolder: GameGridViewHolder, position: Int) {
        val data = displayDataSet.filter(filterFunc)[position]
        viewHolder.bindView(data)
    }

}
