package edu.dhbw.emojisudoku.view.gameList

import android.annotation.SuppressLint
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import edu.dhbw.emojisudoku.dataModel.PersistentSudoku
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.view.gameGrid.GameView
import edu.dhbw.emojisudoku.presenter.PreviewPresenter
import kotlin.math.absoluteValue

class GameGridViewHolder(val view: GameView, private val presenter: PreviewPresenter) : RecyclerView.ViewHolder(view) {

    private val flingGestureListener = object : GestureDetector.SimpleOnGestureListener() {
        lateinit var currentSudoku: SudokuModel
        override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
            //fling upwards
            if ((velocityY * 4).absoluteValue > velocityX && (velocityY).absoluteValue > 0) {
                //DO STUFF: TODO
            }
            return super.onFling(e1, e2, velocityX, velocityY)
        }
    }
    val gestureDetector = GestureDetectorCompat(view.context, flingGestureListener);

    @SuppressLint("ClickableViewAccessibility")
    fun bindView(sudoku: SudokuModel) {

        flingGestureListener.currentSudoku = sudoku

        view.recycle(sudoku)

        view.setOnClickListener { v ->
            presenter.onSelected(sudoku)
        }

        view.setOnTouchListener { _, event ->
            gestureDetector.onTouchEvent(event)
            false
        }

    }
}