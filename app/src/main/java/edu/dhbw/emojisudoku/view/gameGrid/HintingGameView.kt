package edu.dhbw.emojisudoku.view.gameGrid

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import edu.dhbw.emojisudoku.dataModel.NumberEmojiMappings
import edu.dhbw.emojisudoku.dataModel.SudokuData
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import edu.dhbw.emojisudoku.util.Optional
import edu.dhbw.emojisudoku.util.asOptional
import io.reactivex.subjects.PublishSubject

@SuppressLint("CheckResult")
class HintingGameView(private val model: SudokuModel?, context: Context, attributeSet: AttributeSet?)
    : GameView(model, context, attributeSet) {
    constructor(context: Context) : this(null, context, null)


    //set this and selected cells will be send to this channel
    var selectedCellSubject: PublishSubject<Optional<Int>>? = null

    private var mappings = model?.mappings ?: NumberEmojiMappings.linear()


    private val selectedCellPaint = Paint().apply { color = Color.GRAY }
    private val errorPaint = Paint().apply { color = Color.RED; alpha = 50 }


    private var sudokuData = model?.data ?: SudokuData.zeros()
    private var selectedCellIdx: Int? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null || event.actionMasked != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event)

        val idx = cellRectangles.indexOfFirst { it.contains(event.x, event.y) }

        if (idx == -1) return super.onTouchEvent(event)      //touch not in any square

        selectedCellIdx = if (idx == selectedCellIdx || !sudokuData.getCell(idx).editable) null else idx

        selectedCellSubject?.onNext(selectedCellIdx.asOptional())
        invalidate()
        return true
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas!!)

        if (selectedCellIdx != null) {
            canvas.drawRect(cellRectangles[selectedCellIdx!!], selectedCellPaint)
        }

        val columnsDuplicates = sudokuData.duplicateColumns()
                .indexWhere { !it }
        val markedColumns = columnsDuplicates
                .flatMap { column -> (0 until sudokuData.size).map { n -> column * sudokuData.size + n } }

        val rowDuplicates = sudokuData.duplicateRow()
                .indexWhere { !it }

        val markedRows = rowDuplicates
                .flatMap { column -> (0 until sudokuData.size).map { n -> n * sudokuData.size + column } }

        cellRectangles.forEachIndexed { index, rect ->
            if (index in markedColumns ) {
                canvas.drawRect(rect, errorPaint)
            }
            if (index in markedRows){
                canvas.drawRect(rect, errorPaint)
            }

            //duplicated to redraw text on top of markings again
            drawCenteredCellText(canvas, rect, mappings.getString(sudokuData.getCell(index)),
                    if (sudokuData.getCell(index).editable) textPaint else fixedBackgroundPaint)
        }
    }

    fun <E : Any> List<E>.indexWhere(condition: (E) -> Boolean): List<Int> {
        return this.mapIndexed { i, e -> if (condition(e)) i else null }.filterNotNull()
    }


}