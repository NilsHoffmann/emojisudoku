package edu.dhbw.emojisudoku.view.gameGrid

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import edu.dhbw.emojisudoku.R
import edu.dhbw.emojisudoku.dataModel.NumberEmojiMappings
import edu.dhbw.emojisudoku.dataModel.SudokuData
import edu.dhbw.emojisudoku.dataModel.SudokuModel
import java.lang.Math.max
import java.lang.Math.sqrt

@SuppressLint("CheckResult")
open class GameView(private var model: SudokuModel?, context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {

    constructor(context: Context, attributeSet: AttributeSet) : this(null, context, attributeSet)
    constructor(context: Context) : this(null, context, null)


    private var sudokuData = model?.data ?: SudokuData.zeros()
    private var mappings = model?.mappings ?: NumberEmojiMappings.random()

    init {
        model?.sudokuChangedSubject?.subscribe { newData ->
            sudokuData = newData
            invalidate()
        }
    }

    fun recycle(newModel: SudokuModel) {
        this.model = newModel
        this.sudokuData = newModel.data
        this.mappings = newModel.mappings
        invalidate()
    }


    //Make sure the View is square
    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val size = when{
            widthMode == MeasureSpec.UNSPECIFIED -> heightMeasureSpec
            heightMode == MeasureSpec.UNSPECIFIED -> widthMeasureSpec
            else -> if (width > height) heightMeasureSpec else widthMeasureSpec
        }

        setMeasuredDimension(size, size)
    }

    private val rows = sudokuData.size
    private val nSquares = sqrt(rows.toDouble()).toInt()
    private val gapFactor = 0.1F


    protected val cellRectangles = Array(sudokuData.linear.size) { RectF() }
    private val tmpRectF = RectF() //avoid object allocation inside onDraw

    private val backGroundPaint = Paint().apply { color = Color.LTGRAY }

    protected val textPaint = Paint().apply { color = Color.BLACK; }
    protected val fixedBackgroundPaint = Paint().apply { color = Color.parseColor("#ffff8800"); alpha = 80}
    private val squaresPaint = Paint().apply { color = Color.BLACK; style = Paint.Style.STROKE }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas!!)
        val width = canvas.clipBounds.width()

        val widthCell = width / rows.toFloat() * (1 - gapFactor)
        val widthGap = (width / rows.toFloat() * gapFactor * 0.5F)
        val widthTotal = width / rows.toFloat()


        //draw all the cells
        for (x in 0 until rows) {
            for (y in 0 until rows) {
                val left = canvas.clipBounds.left + y * widthTotal
                val top = canvas.clipBounds.left + x * widthTotal
                val drawnRect = cellRectangles[y * sudokuData.size + x]
                drawnRect.left = left + widthGap
                drawnRect.right = drawnRect.left + widthCell
                drawnRect.top = top + widthGap
                drawnRect.bottom = drawnRect.top + widthCell

                canvas.drawRect(drawnRect, backGroundPaint)

                if(!sudokuData.getCell(x,y).editable){
                    canvas.drawRect(drawnRect, fixedBackgroundPaint)
                }

                drawCenteredCellText(canvas, drawnRect, mappings.getString(sudokuData.getCell(x, y)),
                        textPaint)

            }
        }

        drawSquares(canvas, squaresPaint, widthGap)

    }

    private fun drawSquares(canvas: Canvas, paint: Paint, widthGap: Float, condition: (Int) -> Boolean = { true }) {
        //draw the squares
        val offset = canvas.clipBounds.width() / nSquares
        for (idx in 0 until nSquares) {
            for (idy in 0 until nSquares) {
                tmpRectF.top = canvas.clipBounds.top.toFloat() + idy * offset
                tmpRectF.left = canvas.clipBounds.left + idx * offset.toFloat()
                tmpRectF.right = canvas.clipBounds.left + (idx + 1) * offset.toFloat()
                tmpRectF.bottom = canvas.clipBounds.top + (idy + 1) * offset.toFloat()
                squaresPaint.strokeWidth = widthGap
                if (condition(idx * nSquares + idy)) {
                    canvas.drawRect(tmpRectF, paint)
                }
            }
        }

    }


    protected fun drawCenteredCellText(canvas: Canvas, bounds: RectF, text: String, textPaint: Paint) {
        val textBounds = RectF(bounds)
        //TODO configure in settings
        textPaint.textSize = bounds.height() / 1.8F
        textBounds.right = textPaint.measureText(text, 0, text.length);
        // measure text height
        textBounds.bottom = textPaint.descent() - textPaint.ascent();

        textBounds.left += (bounds.width() - textBounds.right) / 2.0f;
        textBounds.top += (bounds.height() - textBounds.bottom) / 2.0f
        canvas.drawText(text, textBounds.left, textBounds.top - textPaint.ascent(), textPaint)
    }

}