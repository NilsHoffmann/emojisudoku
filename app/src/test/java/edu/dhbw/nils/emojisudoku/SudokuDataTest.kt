package edu.dhbw.nils.emojisudoku

import edu.dhbw.emojisudoku.dataModel.parseString
import org.junit.Test

class SudokuDataTest {


    @Test(expected = IllegalArgumentException::class)
    fun testParseStringException() {
        parseString("...123124514")
    }

    @Test
    fun testParsestring() {
        val sudoku = parseString("1.2......1.......................................................................")
        val cell1 = sudoku.grid[0][0]
        val cell2 = sudoku.grid[0][1]
        val cell3 = sudoku.grid[0][2]
        val cell4 = sudoku.grid[1][0]

        assert(!cell1.editable && cell1.possibleValues == setOf(1) && cell1.onePossible && cell1.selected == 1)

        assert(cell2.editable)
        assert(cell2.possibleValues == (0..9).toSet())
        assert(!cell2.onePossible)
        assert(cell2.selected == null)

        assert(!cell3.editable && cell3.possibleValues == setOf(2) && cell3.onePossible && cell3.selected == 2)
        assert(!cell4.editable && cell4.possibleValues == setOf(1) && cell4.onePossible && cell4.selected == 1)
    }


}