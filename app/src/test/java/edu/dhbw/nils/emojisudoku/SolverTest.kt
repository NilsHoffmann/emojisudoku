package edu.dhbw.nils.emojisudoku

import edu.dhbw.emojisudoku.dataModel.SudokuData
import edu.dhbw.emojisudoku.dataModel.SudokuSolver
import edu.dhbw.emojisudoku.dataModel.parseString
import org.junit.Test


class SolverTest {

    @Test
    fun testSolverEmpty(){
        val s = SudokuData()
        val solved = SudokuSolver.solve(s)
        assert(solved.second)
        assert(solved.first!!.fullySelected())
        assert(solved.first!!.possible())
    }

    @Test
    fun testSolver(){
        val s = parseString("1.2..............1...............................................................")
        val solved = SudokuSolver.solve(s)
        assert(solved.second)
        assert(solved.first!!.fullySelected())
        assert(solved.first!!.possible())

    }
}