package edu.dhbw.nils.emojisudoku

import edu.dhbw.emojisudoku.dataModel.parseString
import org.junit.Test

const val valid = ".8......3.2.3....8........7..45...7.....3.2....................4...7............."

class SudokuValidTest {

    @Test
    fun testValid() {
        val s = parseString(valid)
        val duplicates = s.duplicateColumns()
        assert(duplicates.all { true })

        assert(s.duplicateRow().all { true })
        assert(s.duplicateSquares().all { true })
    }

    @Test
    fun testColumns() {
        val test = ".8.....83.2.3....8........7..45...7.....3.2....................4...7............."
        val s = parseString(test)

        assert(!s.duplicateRow()[0])
        assert(s.duplicateRow().subList(1, 9).all { true })
        assert(s.duplicateColumns().all { true })
        assert(s.duplicateSquares().all { true })
    }

    @Test
    fun testRows() {
        val test = ".8......3.8.3....8........7..45...7.....3.2....................4...7............."
        val s = parseString(test)

        assert(!s.duplicateColumns()[1])
        assert(!s.duplicateSquares()[0]!!)
    }

    @Test
    fun testSomething() {
        val test = "1.............1.......5....9.8.........2..122.....3351....8....4...9..4.67......."
        val s = parseString(test)

        assert(s.duplicateColumns() == listOf(true, true, true, true, true, true, true, true))
        assert(!s.duplicateSquares()[5]!!)
    }

}